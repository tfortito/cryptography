def euclalg(a, b,x,y): 
     
    if a == 0 :  
        x = 0
        y = 1
        return b 
          
    x1 = 1
    y1 = 1  
    gcd = euclalg(b%a, a, x1, y1) 
  
    # Update x and y using results of recursive 
    # call 
    x = y1 - (b/a) * x1 
    y = x1 
  
    return gcd 

euclalg(845,117,1,1) 
euclalg(198969098806543209877,1234567890123456233345,1,1) 
euclalg(123456789143333,1234567890123456233345,1,1) 